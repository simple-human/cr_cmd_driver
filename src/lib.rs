//! # cr_cmd_driver
//! 
//! A data source module for control\_room library.
//! It allows simple fetching data from shell commands.
//! 
//! ```rust
//!     use control_room::{DriverRegister, SourcesConfig, Observables};
//!     use cr_cmd_driver::CmdDriver;
//!     use serde_json::json;
//! 
//!     let drivers: DriverRegister = Default::default();
//!     drivers.register::<CmdDriver>("cmd_source".into());
//! 
//!     let sources_config: SourcesConfig = serde_json::from_value(json!(
//!         {"echo": {
//!                      "driver": "cmd_source",
//!                      "options": {
//!                         "cmd": r#"{command} | jq -R --slurp ."#
//!                      }
//!                  }}
//!     ))
//!     .unwrap();
//! 
//!     let observables: Observables = serde_json::from_value(json!(
//!         {
//!             "date": {
//!                 "datasets": [{
//!                     "source": "cmd_source",
//!                     "view_options": {
//!                         "name": "date and time"
//!                     },
//!                     "data_options": {
//!                         "interval": "10 s",
//!                         "args": {
//!                             "command": "date +%F"
//!                         }
//!                     }
//!                 }],
//!                 "client": {}
//!             }
//!         }
//!     ))
//!     .unwrap();
//! ```
//! 
//! The source option *cmd* is a command template with '{variable}' placeholders. 
//! 
//! Observable dataset options are
//! - *interval* - command execution period in [humantime](https://docs.rs/humantime) format;
//! - *args* - a list of placeholders listed in *cmd* template.
//! 
//! There are no any client side controls for obvious security reasons.
//!

use control_room::{Configurator, Driver, Error};
use futures::stream::BoxStream;
use futures::stream::StreamExt;
use serde_derive::Deserialize;
use serde_json::Value;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::time;

use std::process::Command;

#[derive(Debug, Deserialize, Clone)]
pub struct CmdDriver {
    cmd: String,
}

#[derive(Debug, Deserialize, Clone)]
struct Options {
    #[serde(default)]
    args: HashMap<String, String>,
    interval: Option<String>,
}

impl Driver for CmdDriver {
    fn observable(
        &self,
        options: Arc<Value>,
        _controls: Arc<Value>,
    ) -> Result<BoxStream<'static, Result<Value, Error>>, Error> {
        let options =
            serde_json::from_value::<Options>((*options).clone()).map_err(|e| {
                Error::DriverError(format!(
                    "Bad options for CmdDriver: {}\n cmd: {}, Options: {:?}",
                    e, &self.cmd, &options
                ))
            })?;
        let cmd = strfmt::strfmt(&self.cmd, &options.args).map_err(|e| {
            Error::DriverError(format!(
                "Bad options for CmdDriver: {}\n cmd: {}, Options: {:?}",
                e, &self.cmd, &options
            ))
        })?;

        let stream = if let Some(input) = options.interval {
            let parsed = input
                .parse::<humantime::Duration>()
                .map_err(|e| Error::DriverError(format!("can't parse interval: {}", e)))?;

            time::interval(parsed.into())
                .map(move |_| exec(&cmd))
                .boxed()
        } else {
            futures::stream::once(async move { exec(&cmd) }).boxed()
        };

        Ok(stream)
    }
}

fn exec(cmd: &str) -> Result<Value, Error> {
    let output = Command::new("sh").arg("-c").arg(cmd).output();
    output
        .map_err(|e| Error::DriverError(format!("can't execute driver: {}", e)))
        .and_then(|output| {
            if output.status.success() {
                let output = String::from_utf8_lossy(&output.stdout);
                serde_json::from_str::<Value>(&output).map_err(|e| {
                    Error::DriverError(format!(
                        "can't parse driver output to json: {}\ncmd: {:?}",
                        e, cmd
                    ))
                })
            } else {
                // [TODO]: Think about errlog for bad command
                Err(Error::DriverError(format!(
                    "can't execute command: {}",
                    cmd
                )))
            }
        })
}

impl Configurator for CmdDriver {
    fn configure(options: Value) -> Result<Box<dyn Driver>, Error> {
        let driver: CmdDriver =
            serde_json::from_value(options).map_err(|e| Error::DriverError(format!("{}", e)))?;
        Ok(Box::new(driver))
    }
}

#[cfg(test)]
#[allow(unused_must_use)]
mod test {
    use super::*;
    use control_room::Configurator;
    use serde_json::json;
    use tokio::runtime::Runtime;
    use std::sync::Arc;
    use actix::prelude::*;
    use std::thread;
    use std::sync::mpsc::channel;

    #[test]
    fn driver_test() -> Result<(), String> {
        let configure_options = json! (
            {
                "cmd": r#"echo '"{text}"'"#
            }
        ); 

        let driver = <CmdDriver as Configurator>::configure(configure_options)
            .map_err(|e| format!("Can't create Driver: {}", e))?;

        let observable_options = json! ({
            "args": {
                "text": "some text"
            }
        });
        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!(null)))
            .map_err(|e| format!("Can't create observable: {}", e))?;
        let mut rt = Runtime::new().unwrap();
        let result = rt.block_on( async move {
            stream.next().await
        })
        .expect("empty stream")
            .map_err(|e| format!("Can't fetch data: {}", e))?;
        assert_eq!(result, json!("some text"));


        let (sender, receiver) = channel();

        thread::spawn( move || {
            System::run(move || {
                actix::Arbiter::spawn(async move {
                    sender.send(helper().await).expect("can't send msg");
                });
            });
        });

        let result = receiver.recv().expect("can't receive msg")?;

        assert_eq!(result, json!("other text"));

        Ok(())
    }

    async fn helper() -> Result<Value, String> {
        let configure_options = json! (
            {
                "cmd": r#"echo '"{text}"'"#
            }
        ); 

        let driver = <CmdDriver as Configurator>::configure(configure_options)

            .map_err(|e| format!("Can't create Driver: {}", e))?;
        let observable_options = json! ({
            "args": {
                "text": "other text"
            },
            "interval": "10 ms"
        });
        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!(null)))
            .map_err(|e| format!("Can't create observable: {}", e))?;

        stream.next().await
            .ok_or(format!("empty stream"))?;
        let result = stream.next().await
            .ok_or(format!("empty stream"))?
            .map_err(|e| format!("Can't fetch data: {}", e));
        result
    }
}
