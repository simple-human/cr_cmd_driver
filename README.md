# cr_cmd_driver

A data source module for [control\_room](https://gitlab.com/simple-human/control_room) library.
It allows simple fetching data from shell commands.

```rust
use cr_server::{DriverRegister, SourcesConfig, Observables};
use cr_cmd_driver::CmdDriver;
use serde_json::json;

let drivers: DriverRegister = Default::default();
drivers.register::<CmdDriver>("cmd_source".into());

let sources_config: SourcesConfig = serde_json::from_value(json!(
    {"echo": {
                 "driver": "cmd_source",
                 "options": {
                    "cmd": r#"{command} | jq -R --slurp ."#
                 }
             }}
))
.unwrap();

let observables: Observables = serde_json::from_value(json!(
    {
        "date": {
            "datasets": [{
                "source": "cmd_source",
                "view_options": {
                    "name": "date and time"
                },
                "data_options": {
                    "interval": "10 s",
                    "args": {
                        "command": "date +%F"
                    }
                }
            }],
            "client": {}
        }
    }
))
.unwrap();
```

The source option *cmd* is a command template with '{variable}' placeholders. 

Observable dataset options are
- *interval* - command execution period in [humantime](https://docs.rs/humantime) format;
- *args* - a list of placeholders listed in *cmd* template.

There are no any client side controls for obvious security reasons.
